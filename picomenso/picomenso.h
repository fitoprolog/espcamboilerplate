#ifndef __PICOMENSO_H_ 
#define __PICOMENSO_H_ 1 
#define BLOCK_SET_DATA(B,D)  B.nElements = sizeof(D)/sizeof(int32_t);B.data = D;                             
struct ParametersBlock
{
  u_int32_t nElements;   
  float *data; 
  struct ParametersBlock *next;
};

char block_save(struct ParametersBlock *block,const char *fileName);
char block_load(struct ParametersBlock *block, const char *fileName);
char block_from_array(struct ParametersBlock *block,unsigned char *bytes,size_t dataSize);
void block_print(struct ParametersBlock *block);
void block_randomize(struct ParametersBlock *block);
#endif 
